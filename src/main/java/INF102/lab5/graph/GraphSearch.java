package INF102.lab5.graph;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

   

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
         Set<V> visited = new HashSet<>();
        return dfs(u, v, visited);
    }

    private boolean dfs(V start, V target, Set<V> visited) {
        Deque<V> stack = new ArrayDeque<>();
        stack.push(start);

        while (!stack.isEmpty()) {
            V current = stack.pop();

            if (current.equals(target)) {
                return true;
            }

            visited.add(current);

            for (V neighbor : graph.getNeighbourhood(current)) {
                if (!visited.contains(neighbor)) {
                    stack.push(neighbor);
                }
            }
        }

        return false;
    }

    



    


}
